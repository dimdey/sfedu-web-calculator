import React, { useState } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faDeleteLeft } from '@fortawesome/free-solid-svg-icons'
import './App.css';

function App() {
  const [input, setInput] = useState("");
  const [output, setOutput] = useState("");
  const maxInput = 20;
  const symbols = [ "1", "2", "3", "+", "4", "5", "6", "-", "7", "8", "9", "*", "0", "/", "(", ")" ]

  const handleButtonClick = (value) => {
    if (input.length > maxInput)
      return;

    setInput(prev => prev + value);
  };

  const handleClear = () => {
    setInput("");
    setOutput("");
  };

  const handleEvaluate = () => {
    try {
      setOutput(eval(input));
    } catch (e) {
      setOutput("Ошибка");
    }
  };

  const handleRemove = () => {
    setInput(input.slice(0, input.length - 1))
  }
 
  return (
    <div className="container">
      <div className="calculator">
        <div className="display">
          <div className="input">{input}</div>
          <div className="output">{output}</div>
        </div>
        <div className="buttons">
          { symbols.map(symbol => <button onClick={() => handleButtonClick(symbol)}>{symbol}</button>) }
        </div>
        <div className="action-buttons">
          <button className='action__button clear' onClick={handleClear}>C</button>
          <button className='action__button remove' onClick={handleRemove}><FontAwesomeIcon icon={faDeleteLeft} /></button>
          <button className='action__button evaluate' onClick={handleEvaluate}>=</button>
        </div>
      </div>
    </div>
  );
}

export default App;